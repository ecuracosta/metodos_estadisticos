Unidad Temática N° 1 

    Introducción a R y Estadística Descriptiva.
    Test de hipótesis: descripción general.
    Test de hipótesis para la media y la varianza.
    Test de hipótesis para proporción.
    Test de bondad de ajuste.
    Inferencia estadística para dos muestras aleatorias.


Unidad Temática N° 2 

    Métodos de regresión lineal y test de hipótesis.
    Intervalos de confianza para la respuesta media y predicciones futuras.
    Análisis de residuos y coeficiente de determinación.
    Modelo de regresión múltiple y estimación de coeficientes
    Inferencias en regresión múltiple e inferencias basadas en el coeficiente de determinación.
    Predicciones basadas en la regresión múltiple.


Unidad Temática N° 3 

    Diseño de experimentos.
    Análisis de varianza de un solo factor.
    Comparación de medias múltiples.
    Análisis de varianza de varios factores.

Unidad Temática N° 4 

    Estadística no paramétrica.
    Test del signo.
    Test de rangos signados de Wilcoxon.
    Intervalos de confianza sin distribución.
    Métodos no paramétricos en el análisis de varianza (Test de Kruskal-Wallis).
