#!/usr/bin/env Rscript 
#Ejemplo de construcción de Función de distribución acumulada empírica

#A mano
set.seed(641)#Setea la semilla
n<-200
m.normal <- rnorm(n,5,1)
pdf(file = "figura7b.pdf")
plot(sort(m.normal),(1:n)/n,type="s",ylim=c(0,1))

#Con la función ecdf
pdf(file = "figura7c.pdf")
n<-100
m.normal <- rnorm(n,5,1)
Fn <- ecdf(m.normal)
plot(Fn)

#Con la función ecdf utilizando ggplot para graficar
pdf(file = "figura7.pdf")
library(ggplot2)
m.chi <-rchisq(n,4)
datosmios <- data.frame(m.normal,m.chi)#Debe ser data frame
ggplot(datosmios, aes(datosmios$m.normal)) + stat_ecdf(geom = "step")+ 
  labs(x="Muestra.Normal", y="Acumulada empírica")

#Con la función ecdf utilizando ggplot para graficar
pdf(file = "figura8.pdf")
ggplot(datosmios, aes(datosmios$m.chi)) + stat_ecdf(geom = "step")+ 
labs(x="Muestra.Chi", y="Acumulada empírica")
  
pdf(file = "figura9.pdf")
p1<-ggplot(datosmios, aes(sample = m.normal))+ stat_qq() + stat_qq_line()+labs(x="Cuantiles de Normal", y="Cuantiles empíricos")
p1+ggtitle("Datos con distribución Normal")

pdf(file = "figura10.pdf")
p2<-ggplot(datosmios, aes(sample = m.chi))+ stat_qq() + stat_qq_line()+ labs(x="Cuantiles de Normal", y="Cuantiles empíricos")
p2+ggtitle("Datos con distribución Chi")
  
pdf(file = "figura11.pdf")
summary(mtcars)
head(mtcars)
p3 <- ggplot(mtcars, aes(sample = mpg, colour = factor(cyl))) +
  stat_qq() +
  stat_qq_line()  
p3+ggtitle("Comparando consumo de combustible")
  
datospre <- subset(mtcars,mtcars$cyl==4)#Selección condicional
# head(datospre)
datos <-  datospre$mpg
summary(datos)  
sd(datos)
datospre <- subset(mtcars,mtcars$cyl==6)#Selección condicional
# head(datospre)
datos <-  datospre$mpg
summary(datos)
sd(datos)
datospre <- subset(mtcars,mtcars$cyl==8)#Selección condicional
# head(datospre)
datos <-  datospre$mpg
summary(datos)
sd(datos)
# hist(datos,border="black",col="red")  #
# 
# datospre <- subset(mtcars,mtcars$cyl==6)#Selección condicional
# head(datospre)
# datos <-  datospre$mpg
# hist(datos,border="black",col="green")  #
# 
# datospre <- subset(mtcars,mtcars$cyl==8)#Selección condicional
# head(datospre)
# datos <-  datospre$mpg
# hist(datos,border="black",col="blue")  #
