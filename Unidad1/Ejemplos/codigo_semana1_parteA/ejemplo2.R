#!/usr/bin/env Rscript 
#Ejemplos de funciones de distribución de probabilidades discretas y sus 
#funciones de distribución acumulada
pdf(file = "figura3.pdf")
x <- 0:20 #Genera secuencia de números enteros
y<- dbinom(x,size=20,prob=.3)#P(x) binomial B(n=20,p = 0,3)

split.screen(c(2, 3))
screen(1)
plot(x,y,type='h', 
        main="Binomial",
        xlab="x",
        ylab="P(x)")
y<-pbinom(x,size=20,prob=.3)#Fda(x) uniforme
screen(4)
plot(x,y,
        xlab="x",
        ylab="Fda(x)",
        type="s",ylim=c(0,1))

y<-dgeom(x, 0.1)
screen(2)
plot(x,y,type='h', 
        main="Geométrica",
        xlab="x",
        ylab="P(x)")
y<-pgeom(x, 0.1)
screen(5)
plot(x,y,
        xlab="x",
        ylab="Fda(x)",
        type="s",ylim=c(0,1))# 
y<-dpois(x, 10)
screen(3)
plot(x,y,type='h', 
        main="Poisson",
        xlab="x",
        ylab="P(x)")
y<-ppois(x, 10)
screen(6)
plot(x,y,
        xlab="x",
        ylab="Fda(x)",
        type="s",ylim=c(0,1))# 

        
