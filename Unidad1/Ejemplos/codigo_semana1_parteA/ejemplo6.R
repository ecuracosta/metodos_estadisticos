#!/usr/bin/env Rscript 
#Ejemplo de construcción de histogramas
#El histograma se puede utilizar para ver distribuciones
#graficamos con ggplot

set.seed(641)#Setea la semilla
n<-1000
muestra.normal <- rnorm(n,5,1)#Genera 1000 números aleatorios N(5,1)
muestra.chi <-rchisq(n,4)#Genera 1000 números con distribución Chi-cuadrada con 4 dof
datosmios <- data.frame(muestra.normal,muestra.chi)#Debe ser data frame
library(ggplot2)#Importo la librería ggplot2
pdf(file = "figura6.pdf")
ggplot(data=datosmios, aes(datosmios$muestra.normal)) + 
  geom_histogram(aes(y =..density..), 
                 binwidth = .5, 
                 col="black", 
                 fill="blue", 
                 alpha = .2) + 
  geom_density(col=2) + 
  labs(x="Muestra.normal", y="Densidad")
pdf(file = "figura6b.pdf")
ggplot(data=datosmios, aes(datosmios$muestra.chi)) + 
  geom_histogram(aes(y =..density..), 
                 binwidth = .5, 
                 col="black", 
                 fill="blue", 
                 alpha = .2) + 
  geom_density(col=2) + 
  labs(x="Muestra.chi", y="Densidad")

summary(datosmios)

var(datosmios$muestra.normal)
