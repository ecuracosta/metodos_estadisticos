#!/usr/bin/env Rscript 
#Ejemplo de cálculo de descriptores de datos numéricos








#Cargar un set de datos de un archivo
archivo<- "share-of-population-suffering-from-cancer-types.csv" #Path
datos <- read.csv(archivo)#Carga datos del archivo
head(datos)#Muestra un poco
datos.arg <- subset(datos,datos$Entity=='Argentina')#|datos$Entity=='Spain')
datos.esp <- subset(datos,datos$Entity=='Spain')

head(datos.arg)

#http://www.wekaleamstudios.co.uk/supplementary-material/
library(ggplot2)

split.screen(c(2, 1))
screen(1)
ggplot(datos.arg, aes(Year, Liver)) + geom_line() + xlab("Anios") + ylab("Liver")#scale_x_date(format = "%b-%Y")
screen(2)
ggplot(datos.esp, aes(Year, Liver)) + geom_line() + xlab("Anios") + ylab("Liver")#scale_x_date(format = "%b-%Y")






#Datos cargados en una librería
# https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/00Index.html
library(datasets)#Carga la base de datos datasets
data(iris)#Selecciona los de iris
summary(iris)
head(iris)

#Crear mi "data frame"
personas.pre <- c(11,24,10,5,8)
personas.post <- c(5,21,1,2,2)
datosmios <- data.frame(personas.pre,personas.post)
head(datosmios)
summary(datosmios)
# Los guardo en formato CSV
write.csv(datosmios, file = "datosmios.csv")
