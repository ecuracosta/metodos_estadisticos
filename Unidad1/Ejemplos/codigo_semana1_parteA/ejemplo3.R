#!/usr/bin/env Rscript 
#Ejemplo de cálculo de descriptores de datos numéricos

n<-10000
k<-4 #Grados de libertad
y<-rchisq(n, k, ncp = 0) #Genero n números con distribución Ji2 con k grados de libertad

#Media
print('Valor poblacional de media:')
k#Valor teórico
print('Valor muestral de media:')
mean(y)#valor muestral
print('------------------------------')
#Mediana
print('Valor poblacional de mediana:')#Valor teórico
print(k-2/3)
print('Valor muestral de mediana:')
print(median(y))#valor muestral
print('------------------------------')

#Varianza
print('Valor poblacional de la varianza:')#Valor teórico
print(2*k)
print('Valor muestral de la varianza:')
print(var(y))#valor muestral
print('------------------------------')
#Desviación standard
print('Valor poblacional de la desviación standard:')#Valor teórico
print(sqrt(2*k))
print('Valor muestral de la desviación standard:')#Valor teórico
print(sd(y))#valor muestral
print('------------------------------')
#Desviación típica
print('Cuantiles:')
quantile(y)
print('Rango:')
range(y)
print('Rango intercuartílico:')
IQR(y)

print('------------------------------')
print('La función summary muestra:')
summary(y)
print('------------------------------')
print('------------------------------')



#Moda, R no tiene función
#Creo una:
calculomoda <- function(v) {
   uniqv <- unique(v)
   uniqv[which.max(tabulate(match(v, uniqv)))]
}

charv <- c("nose","no","si","si","si","nose")# Creo un vector de palabras
resultado <- calculomoda(charv)# Calculo la moda
print(resultado)
