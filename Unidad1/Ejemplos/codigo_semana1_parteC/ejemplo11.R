#!/usr/bin/env Rscript 
#Ejemplo de t-test

library(ggplot2) 
pdf(file = "figura1.pdf")

#Conversión
cte <- 1.60934/3.78541 #km/l
datosConvertidos <- mtcars
columna<-100/(datosConvertidos$mpg*cte)#Columna convertida
datosConvertidos$mpg <-columna#Datos convertidos
#n pequeño proveniente de Normal
datospre<-subset(datosConvertidos,datosConvertidos$cyl==6)
datos <- datospre$mpg
summary(datos)
cat('n=',length(datos),'\n')

p1 <- ggplot(datospre, aes(sample = mpg)) +
  stat_qq() +
  stat_qq_line()  
p1+ggtitle("Consumo de autos de 6 cilindros")+ labs(x="teórico", y="muestra")


mu0<-11.7#Media de la H0
#Valor muestral del Estadístico
t0<-(mean(datos)-mu0)/(sd(datos)/sqrt(length(datos)))
pvalor<-1-pt(t0,df=length(datos)-1)
#Alternativamente se puede usar:
pvalor<-pt(t0,df=length(datos)-1,lower.tail = FALSE)
cat('Valor muestral del Estadístico:',t0,'\n')
cat('p-valor:',pvalor,'\n')

#Gráficos
pdf(file = "figura3.pdf")
xLims <- c(-4, 4)
right <- seq(t0, xLims[2],   length.out=100)
yH0r  <- dt(right,length(datos)-1)
curve(dt(x,length(datos)-1), xlim=xLims, lwd=2, col="red", xlab="x", ylab="fdp",
       main="Distribución t-student de H0", ylim=c(-0.05, 0.4), xaxs="i")
polygon(c(right, rev(right)), c(yH0r, numeric(length(right))), border=NA,
         density=5, lty=2, lwd=2, angle=45, col="darkgray")       
abline(v=t0, lty=2, lwd=3, col="black")
text(t0+.15, -0.02,expression(t['0']),  cex=1.)
text(t0+.7, 0.05,  'p-valor',  cex=1.3)
alpha <- 0.05
crit <- qt(1-alpha, length(datos)-1)
right2 <- seq(crit, xLims[2],   length.out=100)
yH0r2  <- dt(right2,length(datos)-1)
polygon(c(right2, rev(right2)), c(yH0r2, numeric(length(right2))), border=NA,
         col=rgb(1, 0.3, 0.3, 0.6))
text(crit+.3, 0.025,expression(alpha),  cex=1.3)
         
#R tiene una función para esto:
t.test(datos, y = NULL,
       alternative = "greater",
       mu = 11.7)
